<ul>
<?php

	$limit = isset($_SESSION['count']) ? $_SESSION['count'] : 10;
    for ($i=0; $i < $limit; $i++) {
        ?>
        <li>
            <a href="<?= $rss[$i]->link ?>"><?= $rss[$i]->title ?></a> (<?= parse_url($rss[$i]->link)['host'] ?>)
            <p><?= strftime('%m/%d/%Y %I:%M %p', strtotime($rss[$i]->pubDate)) ?></p>
            <p><?= $rss[$i]->description ?></p>
        </li>
        <?php
    }
?>
</ul>