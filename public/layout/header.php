<!DOCTYPE html>
<html>
<head>
	<title>CatchSmart Trial</title>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="/public/scripts/public.css" type="text/css" />
	<link href="http://allfont.ru/allfont.css?fonts=lucida-sans-unicode" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="/public/scripts/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="/public/scripts/main.js"></script>
</head>
<body>
	<div id="page-wrapper">
		<div id="content">
			<?php if(isset($_SESSION['fb_id'])): 
				if (($_GET['action']) === 'profile') : ?>
					<div class="index-header"><a href="/?action=index">Home</a></div>
				<?php else : ?>
					<div class="index-header"><a href="/?action=profile">Profile</a></div>
				<?php endif; ?>
				<div class="logout-header"><a href="/?action=logout">Logout</a></div>
			<?php endif; ?>
		</div>
		<div id="content">