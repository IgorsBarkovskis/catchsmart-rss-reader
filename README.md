# CatchSmart RSS Reader

Izstrādes nosacījumi:
- uzstāsīts uz GIT repo;
- HTML var brīvi izvēlēties
- Var izvēlēties jebkuru gatavu PHP Framework vai rakstīt plain PHP;
- Uzstādīšnas instrukcija
- Komentēts kods.

Darba uzdevums:
- Autorizācija ar FB autorizāciju
- Profila sadaļā maināmie lauki: vārds, e-pasts, avatars (100x100 px), Kategorija no Delfiem, ierakstu skaits(default 10);
- Sākuma sadaļa, kur attēlojas ieraksti no Delfi RSS feed, ar konkrēto kategoriju un ierakstu skaitu no profila sadaļas. (https://www.delfi.lv/rss/) default channel=delfi
- Logout

Uzstadīšana

domēman ir jābūt https://catchsmart.trial/

Datubazes konfiguracija atrodas /src/config.php

Pirmas palašanas brīdī automatiski tiks izveidota gan datubāze gan 'users' tabula ar visiem laukiem