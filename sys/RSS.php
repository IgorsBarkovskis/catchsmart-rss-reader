<?php

class RSS {
	function getRss() {
		$entries = array();

		$xml = simplexml_load_file(RSS_FEED . $_SESSION['channel']);
		$entries = array_merge($entries, $xml->xpath("//item"));

		return $entries;
	}
}