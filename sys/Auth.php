<?php

class Auth {
	protected $fb;
	protected $db;

	function __construct()
	{
		$this->fb = new FB();
		$this->db = new DB();
	}

	function editProfile()
	{
		if ($_POST)
		{
			$this->db->update(
					'users', 
					array(
						'fb_name' => $_POST['fb_name'],
						'fb_email' => $_POST['fb_email'],
						'fb_pic' => $_POST['fb_pic'],
						'channel' => $_POST['channel'],
						'count' => $_POST['count']
					),
					array('%sssss'),
					array(
						'id' => $_SESSION['id'],
						'fb_id' => $_SESSION['fb_id']
					),
					array('%ss')
				);

			foreach ($_POST as $key => $value)
			{
				$_SESSION[$key] = $value;
			}
		}

	}

	function login()
	{
		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $this->fb->getFB()->getOAuth2Client();
		$accessToken = $this->fb->getAccessToken();

		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken($accessToken);

		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId(APP_ID); // Replace {app-id} with your app id
		// If you know the user ID this access token belongs to, you can validate it here
		//$tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration();

		if (! $accessToken->isLongLived())
		{
		  // Exchanges a short-lived access token for a long-lived one
		  try
		  {
		    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
		  } catch (Facebook\Exceptions\FacebookSDKException $e)
		  {
		    echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
		    exit;
		  }

		  echo '<h3>Long-lived</h3>';
		  var_dump($accessToken->getValue());
		}

		$_SESSION['fb_access_token'] = (string) $accessToken;
		$this->fb->getFB()->setDefaultAccessToken($_SESSION['fb_access_token']);

		$this->getUserData();

		return true;
	}

	protected function getUserData()
	{
		try {
			// getting basic info about user
			$profile_request = $this->fb->getFB()->get('/me?fields=name,email');

			//getting user picture
			$requestPicture = $this->fb->getFB()->get('/me/picture?redirect=false&height=100');
			$picture = $requestPicture->getGraphUser();
			$profile = $profile_request->getGraphUser();

			# save the user nformation in session variable
			if ($user = $this->db->select("SELECT * FROM users WHERE fb_id = ?", array($profile->getProperty('id')), array('%d')))
			{
				$user = $user[0];
				foreach ($user as $key => $value) 
					$array[$key] = $value;
				$_SESSION = $array;
			}
			else 
			{	
				$_SESSION['fb_id'] = $profile->getProperty('id');
				$_SESSION['fb_name'] = $profile->getProperty('name');
				$_SESSION['fb_email'] = $profile->getProperty('email');
				$_SESSION['fb_pic'] = $picture['url'];

				$this->db->insert(
					'users', 
					array(
						'fb_id' => $_SESSION['fb_id'],
						'fb_name' => $_SESSION['fb_name'],
						'fb_email' => $_SESSION['fb_email'],
						'fb_pic' => $_SESSION['fb_pic']
					),
					array('%ssss')
				);
			}
		}
		catch(Facebook\Exceptions\FacebookResponseException $e)
		{
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			session_destroy();
			// redirecting user back to app login page
			header("Location: ./");
			exit;
		}
		catch(Facebook\Exceptions\FacebookSDKException $e)
		{
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
	}
}