<?php
include(LOCAL_URL. "/sys/config.php");
include(LOCAL_URL. "/sys/DB.php");
include(LOCAL_URL. "/sys/Auth.php");
include(LOCAL_URL. "/sys/RSS.php");
include(LOCAL_URL. "/sys/FB.php");
include_once(LOCAL_URL. "/sys/Facebook/autoload.php");

class Controller
{
	//vaiables ------------------------
	private $action;
	private $view;
	private $authOnly = false;

	// views ---------------------------
	private function login()
	{
		$FB = new FB();
		$helper = $FB->getFB()->getRedirectLoginHelper();
		$permissions = ['email']; // Optional permissions
		$loginUrl = $helper->getLoginUrl('https://catchsmart.trial/?action=index', $permissions);
		$this->render(['loginUrl' => $loginUrl]);
	}

	private function index()
	{
		$this->authOnly = true;
		
		if(isset($_SESSION['fb_id']))
		{
			$rss = new RSS();
			$this->render(['rss' => $rss->getRss()]);
		}
		else
		{
			$Auth = new Auth();

			if ($Auth->login())
			{
				$rss = new RSS();
				$this->render(['rss' => $rss->getRss()]);
			}	
			else
			{
				header('Location: /?action=login', TRUE, 302);
			}
		}
	}

	private function profile()
	{
		$this->authOnly = true;
		if($_POST)
		{
			$Auth = new Auth();
			$Auth->editProfile();
		}

		$this->render(['data' => $_SESSION]);
	}

	private function logout()
	{
		session_unset();
		header('Location: /?action=login', TRUE, 302);
	}

	private function page404()
	{
		$this->render();
	}

	// ajax functions -----------------------------
	private function save ()
	{
		$File = new File;
		echo json_encode($File->copy_from_temp());
	}

	private function downloadPDF ()
	{
		$Gallery = new Gallery;
		echo $Gallery->generatePDF($_POST['link']);
	}

	// Core functions -------------------------------------------
	private function render(array $vars = array())
	{
		$this->view = ($this->view) ? $this->view : $this->action;
		if (!isset($_SESSION['fb_id']) && $this->authOnly)
			header('Location: /?action=index', TRUE, 302);

		ob_start();
		extract($vars);
		require(LOCAL_URL . "/public/layout/header.php");
		if (file_exists(LOCAL_URL . "/public/".$this->view.".php")) :
			require(LOCAL_URL . "/public/".$this->view.".php");
		endif;
		require(LOCAL_URL . "/public/layout/footer.php");
		ob_end_flush();
	}

	function __construct()
	{
		$this->action = isset($_GET['action']) ? $_GET['action'] : 'login';
		if (!method_exists('Controller', $this->action)):
			exit;
			$this->action = 'page404';
		endif;

		call_user_func(array('Controller', $this->action));
	}
}