<?php
class DB
{
	private $db;

	function __construct($host = DB_HOST, $user = DB_USER, $pass = DB_PASS, $name = DB_NAME)
	{
		// Check MySQL connection
		$link = mysqli_connect($host, $user, $pass);

		// exit if MySQL isn't connected
		if (!$link)
		{
			echo "Can't connect MySQL: " . PHP_EOL;
			echo "errno: " . mysqli_connect_errno() . PHP_EOL;
			echo "error: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}

		// check DB connection and create DB if it's not exist
		if (!mysqli_select_db($link, $name))
		{
			$query = "CREATE DATABASE $name";

			if (!mysqli_query($link, $query))
			{
				echo "Can't connect MySQL database ". $name . ": " . PHP_EOL;
				echo "errno: " . mysqli_connect_errno() . PHP_EOL;
				echo "error: " . mysqli_connect_error() . PHP_EOL;
				mysqli_close($link);
				exit;
			}
			else
			{
				// create `users` table if DB is created 
				$this->db = new mysqli($host, $user, $pass, $name);
				$this->db->set_charset(DB_CHAR_SET);
				$this->db->query(USER_DB_EXPORT_QUERY);
			}	
		}
		else
		{
			// connect to DB
			$this->db = new mysqli($host, $user, $pass, $name);
			$this->db->set_charset(DB_CHAR_SET);
		}
	}

	function __destruct()
	{
		mysqli_close($this->db);
	}

	function make_query($query)
	{
		$result = $this->db->query($query);

		while ( $row = $result->fetch_object() )
		{
			$results[] = $row;
		}

		return $results;
	}

	function insert($table, $data, $format)
	{
		// Check for $table or $data not set
		if ( empty( $table ) || empty( $data ) )
		{
			return false;
		}

		// Cast $data and $format to arrays
		$data = (array) $data;
		$format = (array) $format;

		// Build format string
		$format = implode('', $format); 
		$format = str_replace('%', '', $format);

		list( $fields, $placeholders, $values ) = $this->prep_query($data);

		// Prepend $format onto $values
		array_unshift($values, $format); 

		// Prepary our query for binding
		$stmt = $this->db->prepare("INSERT INTO {$table} ({$fields}) VALUES ({$placeholders})");

		// Dynamically bind values
		call_user_func_array( array( $stmt, 'bind_param'), $this->ref_values($values));

		// Execute the query
		$stmt->execute();

		// Check for successful insertion
		if ( $stmt->affected_rows )
		{
			return true;
		}

		return false;
	}

	function update($table, $data, $format, $where, $where_format)
	{
		// Check for $table or $data not set
		if ( empty( $table ) || empty( $data ) )
		{
			return false;
		}

		// Cast $data and $format to arrays
		$data = (array) $data;
		$format = (array) $format;

		// Build format array
		$format = implode('', $format); 
		$format = str_replace('%', '', $format);
		$where_format = implode('', $where_format); 
		$where_format = str_replace('%', '', $where_format);
		$format .= $where_format;
		
		list( $fields, $placeholders, $values ) = $this->prep_query($data, 'update');

		//Format where clause
		$where_clause = '';
		$where_values = [];
		$count = 0;

		foreach ( $where as $field => $value )
		{
			if ( $count > 0 )
			{
				$where_clause .= ' AND ';
			}

			$where_clause .= $field . '=?';
			$where_values[] = $value;

			$count++;
		}

		// Prepend $format onto $values
		array_unshift($values, $format);
		$values = array_merge($values, $where_values);

		// Prepary our query for binding
		$stmt = $this->db->prepare("UPDATE {$table} SET {$placeholders} WHERE {$where_clause}");

		// Dynamically bind values
		call_user_func_array( array( $stmt, 'bind_param'), $this->ref_values($values));

		// Execute the query
		$stmt->execute();

		// Check for successful insertion
		if ( $stmt->affected_rows )
		{
			return true;
		}

		return false;
	}

	function select($query, $data, $format)
	{	
		//Prepare our query for binding
		$this->db->prepare($query);
		$stmt = $this->db->prepare($query);

		//Normalize format
		$format = implode('', $format); 
		$format = str_replace('%', '', $format);

		// Prepend $format onto $values
		array_unshift($data, $format);

		//Dynamically bind values
		call_user_func_array( array( $stmt, 'bind_param'), $this->ref_values($data));

		//Execute the query
		$stmt->execute();

		//Fetch results
		$result = $stmt->get_result();

		//Create results object
		while ($row = $result->fetch_object())
		{
			$results[] = $row;
		}

		return isset($results) ? $results : null;
	}

	function delete($table, $id)
	{
		// Prepary our query for binding
		$stmt = $this->db->prepare("DELETE FROM {$table} WHERE ID = ?");

		// Dynamically bind values
		$stmt->bind_param('d', $id);
		
		// Execute the query
		$stmt->execute();

		// Check for successful insertion
		if ( $stmt->affected_rows )
		{
			return true;
		}
	}

	private function prep_query($data, $type='insert')
	{
		// Instantiate $fields and $placeholders for looping
		$fields = '';
		$placeholders = '';
		$values = array();

		// Loop through $data and build $fields, $placeholders, and $values			
		foreach ( $data as $field => $value )
		{
			$fields .= "{$field},";
			$values[] = $value;

			if ( $type == 'update')
			{
				$placeholders .= $field . '=?,';
			}
			else
			{
				$placeholders .= '?,';
			}
		}
		
		// Normalize $fields and $placeholders for inserting
		$fields = substr($fields, 0, -1);
		$placeholders = substr($placeholders, 0, -1);
		
		return array( $fields, $placeholders, $values );
	}
	private function ref_values($array)
	{
		$refs = array();

		foreach ($array as $key => $value)
		{
			$refs[$key] = &$array[$key]; 
		}

		return $refs; 
	}
}

