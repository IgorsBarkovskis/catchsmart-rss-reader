<?php
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'catchsmart');
define('DB_CHAR_SET', 'utf8');
define('CHAR_SET', 'UTF-8');
define('APP_ID', '355128845219748');
define('APP_SECRET', 'fd5229bcfe5ce4de164d3f61d9020234');
define('RSS_FEED', "http://www.delfi.lv/rss/?channel=");
define('USER_DB_EXPORT_QUERY', 
 "CREATE TABLE `users` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `fb_id` varchar(100) COLLATE utf8_general_ci NOT NULL,
 `fb_name` varchar(50) COLLATE utf8_general_ci NOT NULL,
 `fb_email` varchar(100) COLLATE utf8_general_ci NOT NULL,
 `fb_pic` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
 `channel` varchar(50) COLLATE utf8_general_ci DEFAULT 'delfi',
 `count` int(11) DEFAULT 10,
 `created`  timestamp DEFAULT now(),
 `modified` timestamp DEFAULT now() ON UPDATE now(),
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci");